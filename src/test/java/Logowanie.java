import com.codeborne.selenide.Condition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.close;

public class Logowanie {

    private String username = "pawel.stachurski@klgsolutions.com";
    private String password = "Test123#";
    private String facebookUsername = "omnis-fb@mail-lab.net";
    private String facebookPassword = "Test123#";

    @Before
    public void getDriver() {
        SetUp.getDriver();
    }

    @After
    public void tearDown(){
        close();
    }

//    @Test
//    public void prawidłoweLogowanie() throws InterruptedException {
//
//        //najedź na ikone i kliknij 'zaloguj'
//        $(By.xpath("//*[@class='account desktop-only']")).hover();
//        $(By.xpath("//a[@aria-label='Zaloguj się']")).shouldBe(Condition.exist).click(); //FIXME selector
////        (//*[@class='account desktop-only']/*/*)[3]/*/*/*[1]  //też słabo
//
//        //wprowadz poprawne dane i kliknij 'zaloguj
//        $(By.id("username")).shouldBe(Condition.visible).setValue(username);
//        $(By.id("password")).setValue(password);
//        $(By.id("kc-login")).click();
//
//        //sprawdź czy logowanie się udało poprzez sprawdzenie czy wyświetlane są elementy dostępne dla zalogowanego użytkownika
//        $(By.xpath("//div[@class='content'][contains(.,'" + username + "')]")).shouldBe(Condition.visible);
//
//    }
//
//    @Test
//    public void facebookLogowanie() throws InterruptedException {
//
//        //najedź na ikone i kliknij 'zaloguj'
//        $(By.xpath("//*[@class='account desktop-only']")).hover();
//        $(By.xpath("//a[@aria-label='Zaloguj się']")).shouldBe(Condition.exist).click(); //FIXME selector
//
//        //przejdź do logowania przez facebooka
//        $(By.xpath("//span[@class='zocial-label']")).shouldBe(Condition.visible).click();
//
//        //zaloguj się przez facebooka
//        $(By.xpath("//input[@name='email']")).shouldBe(Condition.visible).setValue(facebookUsername);
//        $(By.xpath("//input[@name='pass']")).setValue(facebookPassword);
//        $(By.xpath("//button[@name='login']")).click();
//
////        //część która wyświetla się tylko przy pierwszym logowaniu przez konto facebooka (finalizacja instalacji)
////
////        //potwierdź logowanie
////        $(By.xpath("//button[@name='__CONFIRM__']")).shouldBe(Condition.visible).shouldHave(Condition.text("Kontynuuj jako")).click();
////
////        //sprawdzenie czy wyświetlone zostaje potwierdzenie regulaminu
////        $(By.xpath("//div[@class='card-pf  ']")).shouldBe(Condition.visible);
////        $(By.xpath("//input[@name='accept']")).shouldBe(Condition.visible).click();
//
//        //sprawdź czy logowanie się udało poprzez sprawdzenie czy wyświetlane są elementy dostępne dla zalogowanego użytkownika
//        $(By.xpath("//div[@class='content'][contains(.,'" + facebookUsername + "')]")).shouldBe(Condition.visible);
//    }

    @Test
    public void nieprawidłoweDaneLogowania() throws InterruptedException {

        //najedź na ikone i kliknij 'zaloguj'
        $(By.xpath("//*[@class='account desktop-only']")).hover();
        $(By.xpath("//a[@aria-label='Zaloguj się']")).shouldBe(Condition.exist).click(); //FIXME selector

        //wprowadz prawidłowy email, ale nieprawidłowe hasło i kliknij zaloguj
        $(By.id("username")).shouldBe(Condition.visible).setValue(username);
        $(By.id("password")).setValue(password + "bug");
        $(By.id("kc-login")).click();

        //sprawdź czy wyświetlił się stosowny komunikat
        $(By.xpath("//*[@class='alert alert-error']")).shouldBe(Condition.visible);

        //sprawdź czy logowanie się nieudało poprzez sprawdzenie czy wyświetlane są elementy dostępne dla zalogowanego użytkownika
        $(By.xpath("//a[@aria-label='Wyloguj']")).shouldNotBe(Condition.exist);

        //wprowadz nieprawidłowy email i kliknij 'zaloguj
        $(By.id("username")).shouldBe(Condition.visible).setValue(username + "bug");
        $(By.id("password")).setValue(password);
        $(By.id("kc-login")).click();

        //sprawdź czy wyświetlił się stosowny komunikat
        $(By.xpath("//*[@class='alert alert-error']")).shouldBe(Condition.visible);

        //sprawdź czy logowanie się nieudało poprzez sprawdzenie czy wyświetlane są elementy dostępne dla zalogowanego użytkownika
        $(By.xpath("//a[@aria-label='Wyloguj']")).shouldNotBe(Condition.exist);
    }

}
