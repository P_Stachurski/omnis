import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.junit.runners.Suite.*;


/** Klasa służąca do uruchamiania wszystkich klas z testami, jedna po drugiej
 *  Jeśli chcemy dodać jakąś klasę do runnera wystarczy dopisać ją w @SuiteClasses
 *  Jeśli chcemy uruchomić tylko wybrane klasy, wystarczy je zakomentować w @SuiteClasses
 */

@RunWith(Suite.class)
@SuiteClasses({
        Logowanie.class,
//        Rejestracja.class
})
public class TestSuite {

}
