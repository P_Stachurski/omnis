import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class SetUp {

    public static WebDriver getDriver(){
        Configuration.browser = "firefox";
        Configuration.timeout = 20000;
        open("https://omnis-staging.bn.org.pl");
        getWebDriver().manage().window().maximize();

        return getWebDriver();
    }

    public static WebDriver loginToOmnis(){

        String username = "pawel.stachurski@klgsolutions.com";
        String password = "Test123#";

        Configuration.browser = "firefox";
        Configuration.timeout = 20000;
        open("https://omnis-staging.bn.org.pl");
        getWebDriver().manage().window().maximize();

        //najedź na ikone i kliknij 'zaloguj'
        $(By.xpath("(//span[@class='icon-user-circle'])[2]")).hover();
        $(By.xpath("//a[@aria-label='Zaloguj się']")).shouldBe(Condition.exist).click(); //FIXME selector

        //wprowadz poprawne dane i kliknij 'zaloguj
        $(By.id("username")).shouldBe(Condition.visible).setValue(username);
        $(By.id("password")).setValue(password);
        $(By.id("kc-login")).click();

        //sprawdź czy logowanie się udało poprzez sprawdzenie czy wyświetlane są elementy dostępne dla zalogowanego użytkownika
        $(By.xpath("//div[@class='content'][contains(.,'" + username + "')]")).shouldBe(Condition.visible);

        return getWebDriver();
    }
}
