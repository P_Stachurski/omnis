import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/** Klasa służąca do uruchamiania kolekcji testów
 *  Klasy brane są z klasy TestSuite
 *  W wyniku dostajemy komunikat w konsoli zakończony "True" lub "False"
 */

public class TestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(TestSuite.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println();
        System.out.println("Test Suite execution was successful: "+ result.wasSuccessful());
    }
}
