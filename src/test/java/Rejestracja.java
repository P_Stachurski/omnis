import com.codeborne.selenide.Condition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.close;

public class Rejestracja {

    private String emailAddress = "test ";
    private String password = "test";

    @Before
    public void getDrive(){
        SetUp.getDriver();
    }

    @After
    public void tearDown(){
        close();
    }

    @Test
    public void prawidłowaRejestracja() throws InterruptedException {
        //najedź na ikone i kliknij 'zarejestruj się'
        $(By.xpath("//*[@class='account desktop-only']")).hover();
        $(By.xpath("//a[@aria-label='Zarejestruj się']")).shouldBe(Condition.exist).click(); //FIXME selector

        //wypełni formularz logowania
        $(By.id("email")).shouldBe(Condition.visible).setValue(emailAddress);
        $(By.id("password")).shouldBe(Condition.visible).setValue(password);
        $(By.className("checkmark")).click();

        //CAPTCHA
//        $(By.xpath("//div[@class='recaptcha-checkbox-border']")).click();
        Thread.sleep(3000);

    }
}
